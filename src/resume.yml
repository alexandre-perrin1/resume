name: Alexandre Perrin
birth:
  date: 07-05-1985
  location: Nancy - France
sex: male
drive: Yes
address: |
  105 rue du petit Arbois
  54520 Laxou (France)
tel: +33(0)7 55 61 57 73
email: alexandreperr@gmail.com
github: https://github.com/alexandre-perrin
linkedin: https://www.linkedin.com/in/alexandre-perrin-engineering/
presentation: |
  Creative and curious developer with strong technical skills in embedded systems with real time constraints.
  Independent and up-to-date to relevant technologies and techniques.
  Familiar with CI/CD solutions, Machine Learning and curious about IOT.
languages:
  French: native
  English: fluent
  Hebrew: beginner
  Spanish: basic
experience:
  - company:
      name: Midokura
    positions:
      - title: Senior Software Engineer
        dates: june 2022 - now
        description: |
          Provided expertise on designing software features and maintenance support
        details:
          - Designed features for interraction with WASM programs
          - Refactored features code and improved reliability
          - Improved CI/CD pipelines speed and reliability (GitHub Actions)

  - company:
      name: Self-Employed
    positions:
      - title: Senior Firmware Engineer (freelance - PG Scada)
        dates: sept 2021 - december 2021
        description: |
          Designed Zephyr RTOS based firmware for client SCADA platform
        details:
          - Brought up connectivity drivers of platform for Ethernet/Switch and Telit GE910 Quad-v3 GSM AT modem
          - Wrote basic FTP Client for Zephyr
          - Designed firmware architecture with abstraction through file system interfaces
          - Designed core state machine for scheduling services
      - title: Senior Firmware Engineer (ISIS - Merck Millipore)
        dates: sept 2021 - february 2022
        description: |
          Provided expertise on inter core messaging/communication on bare metal and Zephyr RTOS context
        projects:
          - title: Lead investigations for
            details:
              - Zephyr RTOS integration into current systems
              - bringing up Bluetooth LE on Nora-B106 and multi SoC inter communication
              - evaluation of USB Stack for STM32 device and host (ST Cube library and TinyUSB)
              - Multi Core synchronization / communication model for internal systems
          - title: Developed
            details:
              - USB CDC interfaces to match existing internal serial interfaces
              - drivers for Inter Processor Communication with maximum reuse of internal interfaces

  - company:
      name: Arm
      url: https://www.arm.com
    positions:
      - title: Senior Software Engineer
        dates: april 2020 - july 2021
        description: |
          Brought support for software stack development of future generation of Neural Network Accelerator at all level of stack and improvement of CI/CD processes.
        projects:
          - title: Testing and CI/CD improvements
            details:
              - lead new design of CI/CD process on Jenkins to remove duplication of groovy code
              - simplified groovy implementation and moved logic to external automation scripts
          - title: Abstraction from CI platform with automation tool
            details:
              - investigated and wrote functional requirements for a task execution tool to run on CI platform and user's platform
              - extended PyInvoke functionalities with multiple features (better collection recursive merging, extended pipeline reporting to console, extra settings files, etc.)
          - title: Kernel Driver development
            details:
              - implemented MMU support for dual-core NPU
              - designed device tree for MMU support
          - title: support to APIs libraries
            details:
              - bug fixes
              - multiple feature implementation

  - company:
      name: Merck Millipore
      url: https://www.merckmillipore.com
    positions:
      - title: Control Systems and Embedded Software Expert
        dates: sept 2018 - april 2020
        description: |
          Brought support on the development and deployment of new commercial Milli-Q products (IQ-7000, IQ-70XX, EQ-7000, IX-7000)

        projects:
          - title: Baremetal ARM micro-controllers (STM32) development in C
            details:
              - contributed to the development of the central water process unit firmware based on an OS-less event driven solution (publish-subscibe pattern)
              - contributed to firmware maintenance
          - title: Linux development c/c++
            details:
              - developed applications to communicate with process dedicated micro-controllers
              - developed a File System in User Space
              - contributed to improvement and bug fixes of core server application

          - title: Python development of scripts and libraries
            details:
              - C code generation tool based on COGapp python library
              - CFFI wrappers to test c code with pytest
              - diagnostic toolbox for developers

          - title: Deployment assessment of Continuous Integration/Delivery on Jenkins server
            details:
              - usage of Jenkins pipelines and jenkinsfile script
              - evaluated a c/c++ package dependency manager (conan)
              - evaluated the usage of an artifact server (Artifactory)
              - deployed pipelines for firmwares development branches (test undergoing)

          - title: Deployed a Jupyter notebook server as sandbox for R&D team
            details:
              - Wrote interactive documentations on Jupyter notebook about code implementation to quickly pass knowledge to new developers
              - Writing investigations, development and deployment journals in notebook

      - title: Embedded Software Engineer
        dates: dec 2013 - sept 2018
        description: |
          Pursued development of Medical Device firmware of Baxter's product Viva Hemodialysis System (project abandoned later on), then worked on the development of Milli-Q Type 1 Ultrapure Water product IQ-7000

        projects:
          - title: Bare metal ARM micro-controllers (STM32) development in C
            details:
              - developed a low memory footprint RFID ISO15693 protocol stack
              - developed application firmware of new Milli-Q A10 TOC (Total Organic Carbon) monitor device

          - title: Linux development c/c++
            details:
              - contributed to application services design, development and maintenance
  - company:
      name: Médiane Système
      url: https://www.medianesysteme.com
    title: Embedded Software Engineer
    dates: jul 2010 - dec 2013
    description: |
      Contributed to the development of firmware for OEM Medical Device project for Baxter's product Vivia Hemodialysis System
    details:
      - developed Proof Of Concept tools (C# / LabVIEW)
      - wrote risk assessment with Software of Unknown Pedigree/Commercial off-the-shelf software
      - wrote Functional and Software Design Specifications, and software validation protocol (Installation/Operational Qualification)
      - contributed to embedded firmware development
      - participated to EMC and ESD tests for medical certification at TÜV in Straubing, Germany

  - company:
      name: Hauptman Woodward Institute
      url: https://hwi.buffalo.edu
    title: Intern - Software Engineer
    dates: feb 2009 - mai 2010
    description: |
      Developed a proof of concept with 4-axis robot arm to handle cristallograpy groth experiments and designed a software tool to present crystallographic growth experiment data as plots
    details:
      - programmed Epson robot (SCARA) arm routines with Visual Basic .NET user interface
      - designed mechanical tray and a pick-up tool prototype to handle samples
      - developed a GUI to plot experiment results into charts and compute non-linear fit curves

  - company:
      name: Sagem DS
    title: Intern - Software Developer
    dates: sept 2007 - feb 2008
    description: |
      Developed a new digital electronic control for an inertial vibrating gyro-meter sensor
    details:
      - optimized and redesigned the existing system to reduce cost and improve performances
      - simulated different solutions on MatLab / Simulink
      - developed the chosen Real Time solution on DSC Microchip platform dsPIC33
      - appraised final results and compared performances

education:
  - name: UTBM
    dates: 2006 - 2009
    url: https://www.utbm.fr/
    degree: GESC (Master's)
    field: Electrical, Control Theory, Embedded Systems
    activities: |
      Project Manager of University Robotic Club in 2008 
      Managed a 20 members team divided in 8 sub-projects
      In charge of quality and specifications writing
  - name: Factulté des sciences de Nancy
    field: Electrical, Electronics and Networks
    dates: 2005 - 2006
  - name: IUT du Montet - Nancy
    degree: DUT GEII (Associate's)
    field: Electrical and Industrial Engineering
    dates: 2003 - 2005
  - name: Lycée Saint-Joseph - Laxou
    degree: Highschool diploma - Electronics Engineering (BAC - STI Electronique)
    dates: 2000 - 2003

projects:
  - title: Machine Learning at Google Crash Course
    url: https://developers.google.com/machine-learning/crash-course
    dates: In progress
  - title: Matlab Embedded Coder Course
    url: https://www.mathworks.com/
    dates: June 2019
  - title: Eurobot (European Robotic Cup)
    url: http://www.eurobot.org/
    dates: 2006 - 2007
    description: |
      Participated to the European robotic cup. 
      Worked on developing the ejection process of collected items and assisted the development on the control loop for the robot movements.
  - title: Student film making
    dates: 2003 - 2005
    description: |
      Participated to making a short movie for a suddent non-for-profit organization. 
      Assisted the stage manager and director in various tasks, such as technical deployement, sound taking, visual effects.
