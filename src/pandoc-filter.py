#!/usr/bin/env python3

import mimetypes
import subprocess
import re
import sys
from pathlib import Path
from urllib import parse, request

from pandocfilters import toJSONFilter, Image

FMT_TO_CONVERT = {
    "latex": "pdf",
    "beamer": "pdf",
    "docx": "png",
    "html": "png",
}
RE_REPLACE_NON_PATH_CHARS = re.compile(r"[^a-zA-Z0-9\.]")
RE_MATCH_URL = re.compile(r"https?\://")
DST_DIR = Path("imgs")
MIME_TYPES = ("image/svg+xml",)


def debug(*args, **kwargs):
    kwargs.setdefault("file", sys.stderr)
    print("[pandoc-filter]", *args, **kwargs)


def get_image_meta(value):
    if len(value) == 2:
        # before pandoc 1.16
        alt, [src, title] = value
        attrs = None
    else:
        attrs, alt, [src, title] = value

    mime, _ = mimetypes.guess_type(src)

    return alt, src, title, attrs, mime


def convert_image(dst_dir, value, fmt, meta):
    alt, src, title, attrs, mime = get_image_meta(value)

    if mime not in MIME_TYPES:
        return None

    convert_fmt = FMT_TO_CONVERT.get(fmt)
    if not convert_fmt:
        return None

    src = get_source_image(src, dst_dir)
    dst = src.parent / f"{src.stem}.{convert_fmt}"
    run_inkscape(src, dst, convert_fmt)
    return get_converted_image(attrs, alt, dst, title)


def get_source_image(src, dst_dir):
    if RE_MATCH_URL.match(src):
        scheme, netloc, path, query, fragment = parse.urlsplit(src)
        img_name = parse.unquote(path.strip("/"))
        img_name = RE_REPLACE_NON_PATH_CHARS.sub("-", img_name)
        dst = dst_dir / img_name
        src, _ = request.urlretrieve(src, dst)
    else:
        src = Path(src)

    return src

def run_inkscape(src, dst, fmt):
    cmd_line = ["inkscape", f"--export-{fmt}", dst, src]
    debug(*cmd_line)
    subprocess.call(cmd_line)


def get_converted_image(attrs, alt, dst, title):
    dst = str(dst)
    if attrs:
        return Image(attrs, alt, [dst, title])
    else:
        return Image(alt, [dst, title])


def convert(key, value, fmt, meta):
    if key == "Image":
        return convert_image(DST_DIR, value, fmt, meta)


if __name__ == "__main__":
    toJSONFilter(convert)
