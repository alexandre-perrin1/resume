
# Alexandre Perrin

This repository contains a generation tool to produce my uptodate CV.

A script generates the markdown output based on:
* a YAML data file containing my CV details
* my competancies scores as a csv table
* a JINJA template file for generating the markdown output

The result markdown can be then converted to a mkdocs site for publishing.

The result is available [here](https://alexandre-perrin1.gitlab.io/resume/).
