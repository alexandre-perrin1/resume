
# Alexandre Perrin

![](imgs/skillcloud.png)

## ![](https://fonts.gstatic.com/s/i/materialiconsround/person/v1/24px.svg) Who am I? 

Creative and curious developer with strong technical skills in embedded systems with real time constraints.
Independent and up-to-date to relevant technologies and techniques. 
Familiar with CI/CD solutions, Machine Learning and curious about IOT.


<details><summary>Info</summary>

<p>36 years old</p>

<p><img src="imgs/baby-carriage.png"/>07-05-1985 / Nancy - France</p>

<p><img src="imgs/email.png"/><a href="mailto:alexandreperr@gmail.com">alexandreperr@gmail.com</a></p>

<p><img src="imgs/car.png"/>Yes</p>

<p><img src="imgs/phone.png"/>+33(0)7 55 61 57 73</p>

<p><img src="imgs/linkedin.png"/><a href="https://www.linkedin.com/in/alexandre-perrin-engineering/">https://www.linkedin.com/in/alexandre-perrin-engineering/</a></p>


</details>

You can talk to me in 
**French** I'm native
 or 
**English** I'm fluent
 or 
**Hebrew** I'm beginning

## ![](https://fonts.gstatic.com/s/i/materialiconsround/how_to_reg/v1/24px.svg) What I'm good at...


**Top technical skills:**

* **Python**
* **C/C++**
* **ARM Cortex-M**
* **CI/CD**
* **Real-Time**

<details><summary>More</summary>

<li><b>Jenkins</b></li>
<li><b>Test Automation</b></li>
<li><b>Embedded Systems</b></li>
<li><b>RTOS</b></li>
<li><b>Linux</b></li>
<li><b>Driver</b></li>
<li><b>Software Architecture</b></li>
<li><b>Integration</b></li>
<li><b>LabVIEW</b></li>
<li><b>Jupyter</b></li>
<li><b>Bash Scripting</b></li>
<li><b>Debugging</b></li>
<li><b>JSON</b></li>
<li><b>Simulink</b></li>
<li><b>Kernel Driver</b></li>
<li><b>Matlab</b></li>
<li><b>Comm Protocols</b></li>
<li><b>DevOps</b></li>
<li><b>Control Systems</b></li>
<li><b>Lauterbach</b></li>
<li><b>Testing</b></li>
<li><b>C#</b></li>
<li><b>Medical Devices</b></li>
<li><b>Artifactory</b></li>
<li><b>.NET</b></li>
<li><b>DSP</b></li>
<li><b>dSPACE</b></li>
<li><b>Control Theory</b></li>
<li><b>Robotics</b></li>
<li><b>Machine Learning</b></li>
<li><b>BLE</b></li>
<li><b>Bluetooth</b></li>
<li><b>Electronics</b></li>

</details>

---

## ![](https://fonts.gstatic.com/s/i/materialiconsround/work/v1/24px.svg) What did I do? 

### Self-Employed

**Senior Firmware Engineer (freelance - PG Scada)** *sept 2021 - december 2021*

Designed Zephyr <b>RTOS</b> based firmware for client SCADA platform

<details><summary>Details</summary>

<li>Brought up connectivity <b>driver</b>s of platform for Ethernet/Switch and Telit GE910 Quad-v3 GSM AT modem</li>
<li>Wrote basic FTP Client for Zephyr</li>
<li>Designed firmware architecture with abstraction through file system interfaces</li>
<li>Designed core state machine for scheduling services</li>

</details>

**Senior Firmware Engineer (ISIS - Merck Millipore)** *sept 2021 - now*

Provided expertise on inter core messaging/communication on bare metal and Zephyr <b>RTOS</b> context

</details>
<details><summary>Work</summary>

<dl>
<dt>Lead investigations for</dt>
<dd>Zephyr <b>RTOS</b> <b>integration</b> into current systems</dd>
<dd>bringing up <b>Bluetooth</b> LE on Nora-B106 and multi SoC inter communication</dd>
<dd>evaluation of USB Stack for STM32 device and host (ST Cube library and TinyUSB)</dd>
<dd>Multi Core synchronization / communication model for internal systems</dd>
</dl>
<dl>
<dt>Developed</dt>
<dd>USB CDC interfaces to match existing internal serial interfaces</dd>
<dd><b>driver</b>s for Inter Processor Communication with maximum reuse of internal interfaces</dd>
</dl>
</details>

    


### [Arm](https://www.arm.com)

**Senior Software Engineer** *april 2020 - july 2021*

Brought support for software stack development of future generation of Neural Network Accelerator at all level of stack and improvement of <b>CI/CD</b> processes.

</details>
<details><summary>Work</summary>

<dl>
<dt>Testing and CI/CD improvements</dt>
<dd>lead new design of <b>CI/CD</b> process on <b>Jenkins</b> to remove duplication of groovy code</dd>
<dd>simplified groovy implementation and moved logic to external automation scripts</dd>
</dl>
<dl>
<dt>Abstraction from CI platform with automation tool</dt>
<dd>investigated and wrote functional requirements for a task execution tool to run on CI platform and user's platform</dd>
<dd>extended PyInvoke functionalities with multiple features (better collection recursive merging, extended pipeline reporting to console, extra settings files, etc.)</dd>
</dl>
<dl>
<dt>Kernel Driver development</dt>
<dd>implemented MMU support for dual-core NPU</dd>
<dd>designed device tree for MMU support</dd>
</dl>
<dl>
<dt>support to APIs libraries</dt>
<dd>bug fixes</dd>
<dd>multiple feature implementation</dd>
</dl>
</details>

    


### [Merck Millipore](https://www.merckmillipore.com)

**Control Systems and Embedded Software Expert** *sept 2018 - april 2020*

Brought support on the development and deployment of new commercial Milli-Q products (IQ-7000, IQ-70XX, EQ-7000, IX-7000) 

</details>
<details><summary>Work</summary>

<dl>
<dt>Baremetal ARM micro-controllers (STM32) development in C</dt>
<dd>contributed to the development of the central water process unit firmware based on an OS-less event driven solution (publish-subscibe pattern)</dd>
<dd>contributed to firmware maintenance</dd>
</dl>
<dl>
<dt>Linux development c/c++</dt>
<dd>developed applications to communicate with process dedicated micro-controllers</dd>
<dd>developed a File System in User Space</dd>
<dd>contributed to improvement and bug fixes of core server application</dd>
</dl>
<dl>
<dt>Python development of scripts and libraries</dt>
<dd>C code generation tool based on COGapp <b>python</b> library</dd>
<dd>CFFI wrappers to test c code with pytest</dd>
<dd>diagnostic toolbox for developers</dd>
</dl>
<dl>
<dt>Deployment assessment of Continuous Integration/Delivery on Jenkins server</dt>
<dd>usage of <b>Jenkins</b> pipelines and <b>jenkins</b>file script</dd>
<dd>evaluated a <b>c/c++</b> package dependency manager (conan)</dd>
<dd>evaluated the usage of an artifact server (<b>Artifactory</b>)</dd>
<dd>deployed pipelines for firmwares development branches (test undergoing)</dd>
</dl>
<dl>
<dt>Deployed a Jupyter notebook server as sandbox for R&D team</dt>
<dd>Wrote interactive documentations on <b>Jupyter</b> notebook about code implementation to quickly pass knowledge to new developers</dd>
<dd>Writing investigations, development and deployment journals in notebook</dd>
</dl>
</details>

**Embedded Software Engineer** *dec 2013 - sept 2018*

Pursued development of Medical Device firmware of Baxter's product Viva Hemodialysis System (project abandoned later on), then worked on the development of Milli-Q Type 1 Ultrapure Water product IQ-7000

</details>
<details><summary>Work</summary>

<dl>
<dt>Bare metal ARM micro-controllers (STM32) development in C</dt>
<dd>developed a low memory footprint RFID ISO15693 protocol stack</dd>
<dd>developed application firmware of new Milli-Q A10 TOC (Total Organic Carbon) monitor device</dd>
</dl>
<dl>
<dt>Linux development c/c++</dt>
<dd>contributed to application services design, development and maintenance</dd>
</dl>
</details>

    


### [Médiane Système](https://www.medianesysteme.com)

    
**Embedded Software Engineer** *jul 2010 - dec 2013*

Contributed to the development of firmware for OEM Medical Device project for Baxter's product Vivia Hemodialysis System

<details><summary>Details</summary>

<li>developed Proof Of Concept tools (<b>C#</b> / <b>LabVIEW</b>)</li>
<li>wrote risk assessment with Software of Unknown Pedigree/Commercial off-the-shelf software</li>
<li>wrote Functional and Software Design Specifications, and software validation protocol (Installation/Operational Qualification)</li>
<li>contributed to embedded firmware development</li>
<li>participated to EMC and ESD tests for medical certification at TÜV in Straubing, Germany</li>

</details>


### [Hauptman Woodward Institute](https://hwi.buffalo.edu)

    
**Intern - Software Engineer** *feb 2009 - mai 2010*

Developed a proof of concept with 4-axis robot arm to handle cristallograpy groth experiments and designed a software tool to present crystallographic growth experiment data as plots

<details><summary>Details</summary>

<li>programmed Epson robot (SCARA) arm routines with Visual Basic <b>.NET</b> user interface</li>
<li>designed mechanical tray and a pick-up tool prototype to handle samples</li>
<li>developed a GUI to plot experiment results into charts and compute non-linear fit curves</li>

</details>


### Sagem DS

    
**Intern - Software Developer** *sept 2007 - feb 2008*

Developed a new digital electronic control for an inertial vibrating gyro-meter sensor

<details><summary>Details</summary>

<li>optimized and redesigned the existing system to reduce cost and improve performances</li>
<li>simulated different solutions on <b>MatLab</b> / <b>Simulink</b></li>
<li>developed the chosen Real Time solution on DSC Microchip platform <b>dsP</b>IC33</li>
<li>appraised final results and compared performances</li>

</details>


---

## ![](https://fonts.gstatic.com/s/i/materialiconsround/school/v1/24px.svg) My background 

### [UTBM](https://www.utbm.fr/)

**GESC (Master's)**
*Electrical, Control Theory, Embedded Systems*
*2006 - 2009*
**Activities:**

Project Manager of University Robotic Club in 2008 
Managed a 20 members team divided in 8 sub-projects
In charge of quality and specifications writing


### Factulté des sciences de Nancy

*Electrical, Electronics and Networks*
*2005 - 2006*

### IUT du Montet - Nancy

**DUT GEII (Associate's)**
*Electrical and Industrial Engineering*
*2003 - 2005*

### Lycée Saint-Joseph - Laxou

**Highschool diploma - Electronics Engineering (BAC - STI Electronique)**
*2000 - 2003*


## ![](https://fonts.gstatic.com/s/i/materialiconsround/favorite/v1/24px.svg) What else? 

**Machine Learning at Google Crash Course** *In progress*

</details>

**Matlab Embedded Coder Course** *June 2019*

</details>

**Eurobot (European Robotic Cup)** *2006 - 2007*

Participated to the European robotic cup. 
Worked on developing the ejection process of collected items and assisted the development on the control loop for the robot movements.

</details>

**Student film making** *2003 - 2005*

Participated to making a short movie for a suddent non-for-profit organization. 
Assisted the stage manager and director in various tasks, such as technical deployement, sound taking, visual effects.

</details>

