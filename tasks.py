from invoke import task

@task
def generate_deps(c):
    c.run("pip install -r requirements.generate.txt")

@task(generate_deps)
def generate(c):
    c.run("python src/generate.py docs/index.md")
    
@task
def build_deps(c):
    c.run("pip install -r requirements.deploy.txt")
    
@task(build_deps)
def build(c):
    c.run("mkdocs build -d public")

@task(generate, build, default=True)
def run(c):
    pass